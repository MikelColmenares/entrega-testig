package asf.testing.practica;

import asf.testing.sucesiones.Excepciones.ElementoNoExisteException;

import java.util.*;

public class OfertaHotelera {

    private List<Hotel> hoteles;

    /*
    * Constructor que inicializa la clase a partir de la lista de Hoteles recibida como parámetro
    * */
    public OfertaHotelera(List<Hotel> hoteles) {
        this.hoteles = new ArrayList<>(hoteles);
    }

    /*
    * Devuelve todos los hoteles almacenados en una única colección de tipo List..0
1    * En caso de no haber hoteles deberá devolver una colección vacía.
    * */
    public List<Hotel> obtenerHoteles () {
        return hoteles;
    }

    /**
     * Obtiene un Mapa de los hoteles teniendo como clave la Provincia
     */
    public Map<Provincia, List<Hotel>> obtenerHotelesPorProvincia() {
        Map<Provincia, List<Hotel>> map = new HashMap<Provincia, List<Hotel>>();
        List<Provincia> provincias = new ArrayList<>();
        for(int i=0;i<hoteles.size();i++){
            Boolean distintaProvincia=true;
            for (int j=0;j<provincias.size();j++){

                if(provincias.get(j).getId()==hoteles.get(i).getCiudad().getProvincia().getId()){
                    distintaProvincia=false;
                }
            }
            if(distintaProvincia){
                provincias.add(hoteles.get(i).getCiudad().getProvincia());
            }
        }

        for(int i=0;i<provincias.size();i++){
            List<Hotel> hotelesPorProvincia = new ArrayList<>();
            for (int j=0;j<hoteles.size();j++){
                if (provincias.get(i).getId()==hoteles.get(j).getCiudad().getProvincia().getId()){
                    hotelesPorProvincia.add(hoteles.get(j));
                }
            }
            map.put(provincias.get(i),hotelesPorProvincia);
        }
        return map;
    }

    /*
    * Obtiene una única colección de tipo List con los hoteles pertenecientes a una Ciudad dada
    * En caso de no existir ningún hotel para la ciudad dada, deberá devolver una lista vacía.
    * */
    public List<Hotel> obtenerHotelesDeUnaCiudad(Ciudad ciudad) {
        List<Hotel> hotelesCiudad = new ArrayList<>();
        for (int i=0; i<hoteles.size();i++){
            if(hoteles.get(i).getCiudad().getId()==ciudad.getId()){
                hotelesCiudad.add(hoteles.get(i));
            }
        }
        return hotelesCiudad;
    }

    /*
    * Obtiene un único Hotel por su Id
    * En caso de no existir el Id, deberá devolver una Excepción NoSuchElementException
    *   con el mensaje "Id de Hotel no válido"
    * */
    public Hotel obtenerHotelPorId (Integer id) {
        for (int i = 0; i < hoteles.size(); i++) {
            if (hoteles.get(i).getId() == id) {
                return hoteles.get(i);
            }
        }
        throw new NoSuchElementException("Id de Hotel no válido");
    }

    /*
    * Permite añadir un hotel a la colección
    * */
    public void anyadirHotel(Hotel hotel){
        hoteles.add(hotel);
    }

}
