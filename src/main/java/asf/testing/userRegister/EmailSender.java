package asf.testing.userRegister;

public interface EmailSender {

    boolean sendRegistrationEmail(RegistrationEmail email);

}
