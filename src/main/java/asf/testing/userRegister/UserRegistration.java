package asf.testing.userRegister;

public class UserRegistration {

    private Database database;
    private EmailSender emailSender;

    public UserRegistration(Database database, EmailSender emailSender) {
        this.database = database;
        this.emailSender = emailSender;
    }

    public void registerNewUser(User user) throws UserAlreadyRegisteredException, EmailFailedException {
        if (database.hasUser(user)) {
            throw new UserAlreadyRegisteredException();
        }

        if(!emailSender.sendRegistrationEmail(new RegistrationEmail(user.getEmailAddress()))) {
            throw new EmailFailedException();
        }
        database.addUser(user);
    }

    public void deleteUser(User user) throws UserNotFoundException {
        database.deleteUser(user);
    }


}