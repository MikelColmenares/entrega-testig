package asf.testing.sucesiones;

import asf.testing.sucesiones.Excepciones.ElementoNoExisteException;
import asf.testing.sucesiones.Excepciones.SucesionDemasiadoPequenyaException;

import java.util.List;

public interface Sucesion {

    public void inicializarSucesion(int numeroDeElementos) throws SucesionDemasiadoPequenyaException;

    public Integer getElemento (int posicion) throws ElementoNoExisteException;

    public Integer sumatorioSucesion();

    public Float mediaDeLaSucesion();

    public List<Integer> valoresDeLaSucesion();

}
