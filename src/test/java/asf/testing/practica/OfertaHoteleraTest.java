package asf.testing.practica;

import asf.testing.sucesiones.Excepciones.ElementoNoExisteException;
import asf.testing.sucesiones.Sucesion;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class OfertaHoteleraTest {

    @Test
    public void obtenerHoteles() {
        List<Hotel> hoteles = new ArrayList<>();
        Hotel hotel = Mockito.mock(Hotel.class);
        when(hotel.getDireccion()).thenReturn("Moyua");
        hoteles.add(hotel);
        OfertaHotelera ofertaHotelera = new OfertaHotelera(hoteles);

        assertThat(ofertaHotelera.obtenerHoteles().get(0).getDireccion(),equalTo(hoteles.get(0).getDireccion()));
    }

    @Test
    public void obtenerListaHotelesVacia() {
        List<Hotel> hoteles = new ArrayList<>();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(hoteles);

        assertThat(ofertaHotelera.obtenerHoteles(),hasSize(0));
    }


    @Test
    public void obtenerHotelesPorProvincia() {
        Provincia provincia = Mockito.mock(Provincia.class);
        when(provincia.getId()).thenReturn(1);

        Ciudad ciudad = Mockito.mock(Ciudad.class);
        when(ciudad.getProvincia()).thenReturn(provincia);

        List<Hotel> hoteles = new ArrayList<>();
        Hotel hotel = Mockito.mock(Hotel.class);
        when(hotel.getCiudad()).thenReturn(ciudad);
        hoteles.add(hotel);

        OfertaHotelera ofertaHotelera = new OfertaHotelera(hoteles);

        assertThat(ofertaHotelera.obtenerHotelesPorProvincia().get(provincia),equalTo(hoteles));
    }

    @Test
    public void obtenerHotelesDeUnaCiudad() {
        Ciudad ciudad = Mockito.mock(Ciudad.class);
        when(ciudad.getId()).thenReturn(1);

        List<Hotel> hoteles = new ArrayList<>();
        Hotel hotel = Mockito.mock(Hotel.class);
        when(hotel.getCiudad()).thenReturn(ciudad);
        hoteles.add(hotel);
        OfertaHotelera ofertaHotelera = new OfertaHotelera(hoteles);

        assertThat(ofertaHotelera.obtenerHotelesDeUnaCiudad(ciudad).get(0).getCiudad().getId(),equalTo(ciudad.getId()));
    }

    @Test
    public void obtenerListaVaciaHotelesDeUnaCiudad() {
        Ciudad ciudad = Mockito.mock(Ciudad.class);
        List<Hotel> hoteles = new ArrayList<>();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(hoteles);

        assertThat(ofertaHotelera.obtenerHotelesDeUnaCiudad(ciudad),hasSize(0));
    }

    @Test
    public void obtenerHotelPorId() {
        List<Hotel> hoteles = new ArrayList<>();
        Hotel hotel = Mockito.mock(Hotel.class);
        when(hotel.getId()).thenReturn(1);
        hoteles.add(hotel);

        OfertaHotelera ofertaHotelera = new OfertaHotelera(hoteles);
        assertThat(ofertaHotelera.obtenerHotelPorId(1),equalTo(hotel));
    }

    @Test (expected= NoSuchElementException.class)
    public void obtenerHotelPorIdExcepcion() throws Exception {
        List<Hotel> hoteles = new ArrayList<>();
        OfertaHotelera ofertaHotelera = new OfertaHotelera(hoteles);
        ofertaHotelera.obtenerHotelPorId(1).getId();
        fail();
    }

    @Test
    public void anyadirHotel() {
        List<Hotel> hoteles = new ArrayList<>();
        Hotel hotel = Mockito.mock(Hotel.class);
        when(hotel.getDireccion()).thenReturn("Moyua");
        hoteles.add(hotel);
        OfertaHotelera ofertaHotelera = new OfertaHotelera(hoteles);

        Hotel segundoHotel = Mockito.mock(Hotel.class);
        when(hotel.getDireccion()).thenReturn("Deusto");
        ofertaHotelera.anyadirHotel(segundoHotel);

        assertThat(ofertaHotelera.obtenerHoteles().get(1).getDireccion(),equalTo(segundoHotel.getDireccion()));
    }
}